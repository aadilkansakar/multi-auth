<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = ([
            [
                'name' => 'Aadil Ratna Kansakar',
                'email' => 'aadilkansakar@gmail.com',
                'is_admin' => '1',
                'password' => bcrypt('password'),
            ],
            [
                'name' => 'James Milner',
                'email' => 'jamesmilner@gmail.com',
                'is_admin' => '0',
                'password' => bcrypt('password'),
            ]
        ]);

        User::insert($users);
    }
}
